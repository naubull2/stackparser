/* 
 * File:   main.cpp
 * Author: naubull2
 *
 * Created on July 3, 2015, 7:28 PM
 */

#include <cstdlib>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include "localvar.hpp"         // defines resource file directory (do not commit on the repository)
#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"
#include "rapidxml/rapidxml_utils.hpp"

using namespace std;
using namespace rapidxml;

xml_node<>* findChild(xml_node<>* node, const string& attribute, const string& value);
void removeHTMLTags(const char* filename);

int main(int argc, char** argv) {
    file<> xmlFile(resource_dir.c_str());
    xml_document<> doc;

    FILE* ofile = fopen("qa_pair.txt", "w");

    doc.parse<0>(xmlFile.data());

    xml_node<>* root = doc.first_node(); // The root 
    xml_node<>* current_node = root->first_node(); // holds the first post node
    int pair_idx = 0;
    do {
        /**
         * Check if the current node is a question
         * PostTypeId == 1 : question has [AcceptedAnswerId]
         *               2 : answer has [ParentId]
         */
        if (!strcmp(current_node->first_attribute("PostTypeId")->value(), "1")) {
            // If it has an accepted answer
            if (current_node->first_attribute("AcceptedAnswerId")) {
                pair_idx++;
                string answer_id = current_node->first_attribute("AcceptedAnswerId")->value();

                xml_node<>* answer = findChild(root->first_node(), "Id", answer_id);
                if (answer != NULL) {
                    printf("Answer found! : %d\n", atoi(answer_id.c_str()));
                    
                    // Save in the following format
                    // [qstart] question string [astart] answer string
                    fprintf(ofile, "qstart\n%s\n", current_node->first_attribute("Body")->value());
                    fprintf(ofile, "astart\n%s\n", answer->first_attribute("Body")->value());
                }
                current_node = current_node->next_sibling();
            } else {
                current_node = current_node->next_sibling();
                continue;
            }
        }else {
            current_node = current_node->next_sibling();
            continue;
        }
    // Last post is ignored for convenience
    } while (current_node->next_sibling());

    fclose(ofile);

    cout << "Total " << pair_idx << " Q-A pairs found"<< endl;
    
    // Remove HTML tags
    removeHTMLTags("qa_pair.txt");
    
    
    return 0;
}

/**
 * Finds and returns the first node with the given attribute value
 * @param node head node
 * @param attribute type of the attribute value to search for
 * @param value 
 * @return xml_node pointer to the found node
 */
xml_node<>* findChild(xml_node<>* node, const string& attribute, const string& value) {
    xml_node<>* ret_val = NULL;
    do {
        if (!strcmp(node->first_attribute(attribute.c_str())->value(), value.c_str())) {
            ret_val = node;
            break;
        }
        node = node->next_sibling();
    } while (node->next_sibling());

    return ret_val;
}

void removeHTMLTags(const char* filename) {
    FILE* ifile = fopen(filename, "r");
    FILE* ofile = fopen("qa_clean.txt", "w");
    
    int in_tag = 0;
    char c;

    while ((c = fgetc(ifile)) != EOF) {
        if (c == '<' || c == '>') {
            in_tag = (c == '<') ? 1 : 0;
        } else {
            if (!in_tag) {
                fputc(c, ofile);
            }
        }
    }
    fclose(ofile);
    fclose(ifile);
}